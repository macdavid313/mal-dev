;;;; printer.lisp
;;;; "This file will contain a single function pr_str which does the opposite of read_str:
;;;; take a mal data structure and return a string representation of it."
(in-package :cl-user)
(defpackage mal.printer
  (:use :cl :mal.types)
  (:export :pr-str :string-join)
  (:documentation "Package that contains functions that print mal forms."))
(in-package :mal.printer)

(defun string-join (strs sep &key (before-first "") (after-last ""))
  ;; The original one has another arguments called `before-last`, however,
  ;; I don't think I need that one for this simple case.
  "An incomplete version of same function from Scheme."
  (if (alexandria:emptyp strs)
      ;; when `strs` is a empty sequence, just concatenate `before-first` and `after-last`.
      (concatenate 'string before-first after-last)
    (concatenate 'string
                 before-first
                 (reduce #'(lambda (x y) 
                             (concatenate 'string x sep y)) 
                         strs)
                 after-last)))

(defun print-hash-table (ht &optional print-readably)
  "Print a hash-table."
  (declare (type hash-table ht)
           (ignorable print-readably))
  (let ((keys (alexandria:hash-table-keys ht)))
    (string-join
     (map 'vector 
          #'(lambda (k)
              (format nil "~a ~a" 
                      (pr-str k print-readably) (pr-str (gethash k ht) print-readably)))
	  keys)
     " "
     :before-first "{" :after-last "}")))

(defun pr-str (obj &optional (print-readably t))
  (declare (optimize (space 0) (speed 3) (safety 0)))
  (cond ((or (mal-scalar-p obj) (mal-procedure-p obj))
         (print-mal-data-str obj print-readably))
	;;; macrop ... (it may need to maintain enviroments, I guess ...)
	(;; a hash-table
         (hash-table-p obj) (print-hash-table obj print-readably))
	(;; a hash-table
         (vectorp obj) (string-join
                             (map 'vector #'(lambda (x) (pr-str x print-readably)) (coerce obj 'list))
                             " " :before-first "[" :after-last "]"))
	(;; a list
         (listp obj) (string-join
                           (map 'vector #'(lambda (x) (pr-str x print-readably)) obj)
                           " " :before-first "(" :after-last ")"))
        ;; in case I forgot something ...
	(t (format nil "~a" obj))))
