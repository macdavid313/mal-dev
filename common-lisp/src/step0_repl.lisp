;;;; step0_repl.lisp
(in-package :cl-user)
(defpackage :mal.step0
  (:use :cl :mal.readline)
  (:export :main))
(in-package :mal.step0)

(defun mal-read (str) str)

(defun mal-eval (ast env)
  (declare (ignore env))
  ast)

(defun mal-print (exp) exp)

(defun rep (str)
  (mal-print (mal-eval (mal-read str) "")))

(defun main (&rest args)
  (declare (ignore args))
  (format t "Mal [Common Lisp]~%")
  #+ccl (sleep 0.5)
  (loop for line = (readline)
        while line
        do (progn
             (when (string/= line "")
               (format t "~A~%" (rep line)))
             #+ccl (sleep 0.5)
             ))
  #+ccl (sleep 0.5)
  (format t "~%"))
