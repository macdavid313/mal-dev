;;;; conditions.lisp
;;;; unexpected-token
(in-package :cl-user)
(defpackage mal.conditions
  (:use :cl)
  (:export 
   :mal-source :mal-error-info
   :unmatched-token-error :current-token :expected-token
   :unexpected-token-error 
   :hashmap-value-missed-error
   :env-symbol-not-found-error :env-symbol :env-content
   :eval-not-applicable-error :eval-symbol-function)
  (:documentation "Conditions that will be handled during runtime of Mal."))
(in-package :mal.conditions)

;;; mal error
(define-condition mal-error (error)
  ((source :initarg :source :initform "" :accessor mal-source :type string)
   (error-info :initarg :error-info :initform nil :accessor mal-error-info :type string)))

(defmethod print-object :before ((obj mal-error) stream)
  (let ((source (mal-source obj))
        (error-info (mal-error-info obj)))
    (if (string= source "")
        (format stream "Error: [~a].~%Details: " error-info)
      (format stream "Error: [~a], in '~a'.~%Details: " error-info source))))

;;; reader error
;; unmatched token error
(define-condition unmatched-token-error (mal-error)
  ((current-token :initarg :current-token :reader current-token)
   (expected-token :initarg :expected-token :reader expected-token)))

(defmethod print-object ((obj unmatched-token-error) stream)
  (format stream "unmatched '~a' token found; the current token is '~a'.~%"
          (expected-token obj) (current-token obj)))

(defun unmatched-token-error (current expected &optional (source ""))
  (error 'unmatched-token-error 
         :current-token current
         :expected-token expected
         :source source
         :error-info "Mal Reader Error"))

;; unexpected token error
(define-condition unexpected-token-error (mal-error)
  ((current-token :initarg :current-token :reader current-token)))

(defmethod print-object ((obj unexpected-token-error) stream)
  (format stream "meet an unexpected token: '~a'.~%" (current-token obj)))

(defun unexpected-token-error (current &optional (source ""))
  (error 'unexpected-token-error
         :current-token current
         :source source
         :error-info "Mal Reader Error"))

;;; hashmap errors
(define-condition hashmap-error (mal-error) nil)

(define-condition hashmap-value-missed-error (hashmap-error)
  ((hashmap-key :initarg :hashmap-key :reader hashmap-key)))

(defmethod print-object ((obj hashmap-value-missed-error) stream)
  (format stream "missing value for a key: '~a' within a hashmap.~%" (hashmap-key obj)))

(defun hashmap-value-missed-error (hashmap-key &optional (source ""))
  (error 'hashmap-value-missed-error
         :hashmap-key hashmap-key
         :source source
         :error-info "Mal Hashmap Error"))

;;; env errors
(define-condition mal-env-error (mal-error) 
  ((env-content :initarg :env-content :reader env-content)))

(define-condition env-symbol-not-found-error (mal-env-error)
  ((env-symbol :initarg :env-symbol :reader env-symbol)))

(defmethod print-object ((obj env-symbol-not-found-error) stream)
  (format stream "'~a' not found in current environment: ~a~%"
          (env-symbol obj) (env-content obj)))

(defun env-symbol-not-found-error (env-symbol env-content &optional (source ""))
  (error 'env-symbol-not-found-error
         :env-symbol env-symbol :env-content env-content
         :source source :error-info "Mal Environment Error"))

;;; eval errors
(define-condition mal-eval-error (mal-error) nil)

(define-condition eval-not-applicable-error (mal-eval-error)
  ((eval-symbol-function :initarg :eval-symbol-function :reader eval-symbol-function)))

(defmethod print-object ((obj eval-not-applicable-error) stream)
  (format stream "'~a' is not applicable.~%" (eval-symbol-function obj)))

(defun eval-not-applicable-error (thing &optional (source ""))
  (error 'eval-not-applicable-error 
         :eval-symbol-function thing
         :source source :error-info "Mal Eval Error"))
