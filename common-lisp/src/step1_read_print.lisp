;;;; step1_read_print.lisp
(in-package :cl-user)
(defpackage mal.step1
  (:use :cl :mal.readline :mal.conditions :mal.reader :mal.printer)
  (:export :main))
(in-package :mal.step1)

(defvar +empty-string+ "")

(defun mal-read (str)
  (let ((reader (read-str str)))
    (unwind-protect (read-form reader)
      (unless (reader-eof-p reader)
        (unexpected-token-error (reader-peek reader) str)))))

(defun mal-eval (ast env)
  (declare (ignore env))
  ast)

(defun mal-print (forms)
  (pr-str forms t))

(defun rep (str)
  (mal-print (mal-eval (mal-read str) "")))

(defun repl-loop ()
  (loop for line = (readline)
        while line
        do (when (and (string/= line +empty-string+)
                      (not (comments-line-p line)))
             (let ((result (handler-case (rep line)
                             ;; handle conditions here and return values back to `result`
                             (unmatched-token-error (c)
                               (format t "Error: expected '~a', got ~A~%"
                                       (expected-token c) (current-token c))
                               +empty-string+)
                             ((or 
				 unexpected-token-error
				 hashmap-value-missed-error)
                                     (condition)
                               (setf (mal-source condition) line)
                               (princ condition)
                               +empty-string+)
                             (error (c)
                               ;; print error information and continue to next loop anyhow
                               (print c)
                               (format t "~%")
                               +empty-string+))))
               (when (string/= result +empty-string+)
                 (format t "~A~%" result))
               #+ccl (sleep 0.5)
               )))
  (format t "~%"))

(defun main (&rest args)
  (declare (ignore args))
  (format t "Mal [Common Lisp]~%")
  #+ccl (sleep 0.5)
  (repl-loop))
