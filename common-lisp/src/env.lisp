;;;; env.lisp
(in-package :cl-user)
(defpackage mal.env
  (:use :cl :mal.conditions :mal.types)
  (:export :mal-env :make-env :env-set :env-get :env-find)
  (:documentation "The mal environment definition."))
(in-package :mal.env)

(defstruct mal-env 
  (table (make-hash-table :test 'eq) :type hash-table)
  (outer nil :type (or symbol mal-env))
  (keys '() :type (or symbol list)))

(defun make-env (bindings &key (outer nil outer-p) (exprs nil exprs-p))
  (let ((table (make-hash-table :test 'eq)))
    (labels ((return-env (table)
               (return-from make-env
                 (make-mal-env :table table :outer outer :keys (alexandria:hash-table-keys table))))
             (iter (bindings index exprs end)
               (cond ((= index end) (return-env table))
                     (t (let ((k (aref bindings index)))
                          (cond ((mal-symbol-eq k +mal-&+)
                                 (setf (gethash (get-mal-symbol (aref bindings (+ index 1))) table) exprs)
                                 (iter bindings end nil end))
                                (t (let ((v (car exprs)))
                                     (setf (gethash (get-mal-symbol k) table) v)
                                     (iter bindings (+ index 1) (cdr exprs) end)))))))))
      (cond (;; this is for step3_env
             (not exprs-p)
             (cond ((listp bindings)
                    (let ((env (loop for (k . v) in bindings
                                     do (setf (gethash (get-mal-symbol k) table) v)
                                     finally (return table))))
                      (return-env env)))
                   ((vectorp bindings)
                    (let ((env (loop for kv across bindings
                                     do (setf (gethash (get-mal-symbol (car kv)) table) (cdr kv))
                                     finally (return table))))
                      (return-env env)))
                   ((null bindings) (return-env table))))
            (t ;; this is for step4 and later
               (let* ((bindings (cond ((and (or (listp bindings) (vectorp bindings))
                                            (alexandria:sequence-of-length-p bindings 0))
                                       (return-env table))
                                      ((listp bindings) (coerce bindings 'vector))
                                      ((mal-symbol-eq bindings +mal-empty-list+) nil)
                                      (t bindings)))
                      (end (length bindings)))
                 (iter bindings 0 exprs end)))))))

;;; set, find and get
(defun env-set (env k v)
  "Takes a symbol key and a mal value and adds to the data structure"
  (declare (type mal-env env)
           (type mal-symbol k))
  (progn
    (setf (gethash (get-mal-symbol k) (mal-env-table env)) v)
    (setf (mal-env-keys env) (alexandria:hash-table-keys (mal-env-table env)))
    env))

(defun env-find (env k)
  "Takes a symbol key and if the current environment contains that key then return the environment. If no key is found and outer is not nil then call find (recurse) on the outer environment."
  (declare (type mal-env env)
           (type mal-symbol k))
  (let ((keys (mal-env-keys env))
        (outer (mal-env-outer env)))
    (if (member (get-mal-symbol k) keys :test 'eq)
        env
      (when outer
        (env-find outer k)))))

(defun env-get (env k)
  "Takes a symbol key and uses the find method to locate the environment with the key, then returns the matching value. If no key is found up the outer chain, then throws/raises a \"not found\" error."
  (declare (type mal-env env)
           (type mal-symbol k))
  (let ((target-env (env-find env k)))
    (if target-env
        (values (gethash (get-mal-symbol k) (mal-env-table target-env)))
      (env-symbol-not-found-error (get-mal-symbol k) env))))
