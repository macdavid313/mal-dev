;;;; types.lisp
(in-package :cl-user)
(defpackage mal.types
  (:use :cl :mal.conditions)
  (:export 
   :print-mal-data-str
   :mal-scalar-p
   :mal-symbol :make-mal-symbol :mal-symbol-p :mal-symbol-eq :get-mal-symbol :mal-keyword-p :mal-default-symbol-p
   :+mal-true+ :+mal-false+ :+mal-nil+ :+mal-empty-list+ 
   :+mal-quote+ :+mal-quasiquote+ :+mal-splice-unquote+ :+mal-unquote+ :+mal-with-meta+ :+mal-deref+
   :+mal-def!+ :+mal-let*+ :+mal-&+ :+mal-if+ :+mal-fn*+ :+mal-do+ 
   :+mal-eval+ :+mal-*ARGV*+ :+mal-concat+ :+mal-cons+ :+mal-defmacro!+ :+mal-macroexpand+
   :make-mal-procedure :mal-procedure-p :mal-procedure-regular-p 
   :mal-procedure-is-macro-p :set-mal-procedure-is-macro
   :mal-procedure-ast :mal-procedure-params :mal-procedure-env
   :mal-procedure-fn :mal-procedure-p :apply-mal-procedure))
(defpackage mal
  (:documentation "A package for Mal users."))
(in-package :mal.types)

(defgeneric print-mal-data-str (mal-type &optional print-readably)
  (:documentation "Print out a mal data as a string."))

;;; Mal Maps
;;; e.g. {"1" 2 "3" 4}, {:a 1 :b 2}. The keys should be of type string or keyword.
;;; using Lisp's Hash Tables

;;; Mal Lists & Vectors
;;; using Lisp's lists and vectors

;;; Mal Scalars
(defun mal-scalar-p (thing)
  (or (mal-symbol-p thing) (numberp thing) (stringp thing)))

;; Symbol
(defstruct (mal-symbol (:constructor make-mal-symbol%))
  (string "" :type string)
  (symbol nil :type symbol)
  (keywordp nil :type symbol))

(defun make-mal-symbol (str &key (keywordp nil))
  (declare (type string str))
  (make-mal-symbol% :string str
                    :symbol (intern str :mal)
                    :keywordp keywordp))

(defun mal-keyword-p (thing)
  (and (mal-symbol-p thing) (mal-symbol-keywordp thing)))

;; Default constant symbols
(defvar +mal-true+ (make-mal-symbol "true"))
(defvar +mal-false+ (make-mal-symbol "false"))
(defvar +mal-nil+ (make-mal-symbol "nil"))
(defvar +mal-empty-list+ (make-mal-symbol "()"))
(defvar +mal-quote+ (make-mal-symbol "quote"))
(defvar +mal-quasiquote+ (make-mal-symbol "quasiquote"))
(defvar +mal-splice-unquote+ (make-mal-symbol "splice-unquote"))
(defvar +mal-unquote+ (make-mal-symbol "unquote"))
(defvar +mal-with-meta+ (make-mal-symbol "with-meta"))
(defvar +mal-deref+ (make-mal-symbol "deref"))

;; Some special operators
(defvar +mal-def!+ (make-mal-symbol "def!"))
(defvar +mal-let*+ (make-mal-symbol "let*"))
(defvar +mal-&+ (make-mal-symbol "&"))
(defvar +mal-if+ (make-mal-symbol "if"))
(defvar +mal-fn*+ (make-mal-symbol "fn*"))
(defvar +mal-do+ (make-mal-symbol "do"))
(defvar +mal-eval+ (make-mal-symbol "eval"))
(defvar +mal-*ARGV*+ (make-mal-symbol "*ARGV*"))
(defvar +mal-concat+ (make-mal-symbol "concat"))
(defvar +mal-cons+ (make-mal-symbol "cons"))
(defvar +mal-defmacro!+ (make-mal-symbol "defmacro!"))
(defvar +mal-macroexpand+ (make-mal-symbol "macroexpand"))

(defvar +default-mal-symbols+
  (list +mal-true+ +mal-false+ +mal-nil+ +mal-empty-list+))

(defun mal-default-symbol-p (mal-symbol)
  (declare (type mal-symbol mal-symbol))
  (and (member mal-symbol +default-mal-symbols+ :test 'mal-symbol-eq) t))

(defun mal-symbol-eq (a b)
  (and (mal-symbol-p a) (mal-symbol-p b)
       (eq (mal-symbol-symbol a) (mal-symbol-symbol b))))

(defun get-mal-symbol (mal-symbol)
  (declare (type mal-symbol mal-symbol))
  (mal-symbol-symbol mal-symbol))

(defmethod print-object ((obj mal-symbol) stream)
  (format stream "~a" (mal-symbol-string obj)))

(defmethod print-mal-data-str ((obj mal-symbol) &optional print-readably)
  (declare (ignore print-readably))
  (mal-symbol-string obj))

(defvar +newline+ (string #\Newline))

;; String
(defmethod print-mal-data-str ((obj string) &optional print-readably)
  (if print-readably
      (prin1-to-string obj)
      obj))

;;; Number
(defmethod print-mal-data-str ((obj number) &optional print-readably)
  (declare (ignore print-readably))
  (cond ((integerp obj) (write-to-string obj))
        (t (format nil "~f" obj))))

;;; Mal Procedure
(defvar +mal-procedure-default-name+ "Anonymous Mal Procedure.")

(defstruct mal-procedure
  (fn #'(lambda (x) (declare (ignore x)) nil) :type function)
  (src "" :type string)
  (name +mal-procedure-default-name+ :type string)
  ;; next slots are for step5_tco and later
  (ast nil :type (or symbol cons))
  (params nil :type (or symbol cons))
  (env nil)
  (regular t :type symbol)
  ;; for step8_macro and later
  (is-macro nil :type symbol))

(defun mal-procedure-regular-p (proc)
  (declare (type mal-procedure proc))
  (mal-procedure-regular proc))

(defun mal-procedure-is-macro-p (proc)
  (declare (type mal-procedure proc))
  (mal-procedure-is-macro proc))

(defun set-mal-procedure-is-macro (proc val)
  (declare (type mal-procedure proc)
           (type symbol val))
  (setf (mal-procedure-is-macro proc) val))

(defmethod print-object ((obj mal-procedure) stream)
  (let ((name (mal-procedure-name obj))
        (is-macro (mal-procedure-is-macro obj)))
    (cond (is-macro
           (format stream "#<Mal Macro: ~a>" (mal-procedure-src obj)))
          ((eq name +mal-procedure-default-name+)
           (format stream "#<Mal Anonymous Procedure: ~a>" (mal-procedure-src obj)))          
          (t "#<Mal Procedure: ~a>" name))))

(defmethod print-mal-data-str ((obj mal-procedure) &optional print-readably)
  (declare (ignore print-readably))
  (write-to-string obj))

(defun apply-mal-procedure (mal-procedure params)
  (declare (type mal-procedure mal-procedure))
  (let ((fn (mal-procedure-fn mal-procedure)))
    (apply fn params)))

(defmethod get-mal-procedure (mal-procedure)
  (declare (type mal-procedure mal-procedure))
  (list :fn (mal-procedure-fn mal-procedure) 
        :src (mal-procedure-src mal-procedure) 
        :name (mal-procedure-name mal-procedure)))
