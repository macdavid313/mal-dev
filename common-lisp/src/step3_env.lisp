;;;; env.lisp
(in-package :cl-user)
(defpackage mal.step3
  (:use :cl :mal.readline :mal.conditions :mal.types :mal.env :mal.reader :mal.printer)
  (:export :main))
(in-package :mal.step3)

(defvar +empty-string+ "")

(defun mal-read (str)
  (let ((reader (read-str str)))
    (unwind-protect (read-form reader)
      (unless (reader-eof-p reader)
        (unexpected-token-error (reader-peek reader) str)))))

(defparameter *repl-env*
  (make-env
   `((,(make-mal-symbol "+") . ,#'(lambda (&rest nums) (apply #'+ nums)))
     (,(make-mal-symbol "-") . ,#'(lambda (&rest nums) (apply #'- nums)))
     (,(make-mal-symbol "*") . ,#'(lambda (&rest nums) (apply #'* nums)))
     (,(make-mal-symbol "/") . ,#'(lambda (&rest nums) (apply #'/ nums))))
   :outer nil))

(defun eval-ast (ast env)
  (declare (type mal-env env)
           (optimize (speed 3) (safety 0) (space 0)))
  (cond ((or (numberp ast) (stringp ast)) ast)
	((mal-symbol-p ast)
         (cond (;; default symbols
                (mal-default-symbol-p ast)
                ast)
               ((mal-keyword-p ast) ast)
               (t (env-get env ast))))
        ;; a list
        ((listp ast) (map 'list #'(lambda (x) (mal-eval x env)) ast))
        ;; a vector
        ((vectorp ast)
         (map 'vector #'(lambda (x) (mal-eval x env)) ast))
        ;; a hash-table
        ((hash-table-p ast)
         (maphash #'(lambda (k v) 
                      (setf (gethash k ast) (mal-eval v env))) ast)
         ;; remember to return the hash-table itself!
         ast)
        (t ast)))

(defun parse-let*-params (params toplevel-env)
  "Take a parameters sequence and current environment from 'let*', 
return an enviroment that composed of parameters and set current environment to its outer." 
  (declare (type (or list vector symbol) params)
           (type mal-env toplevel-env))
  (let ((params (if (listp params) (coerce params 'vector) params))
        (bindings (make-array 0 :element-type 'cons :adjustable t :fill-pointer 0)))
    (loop for params-index = 0 then (+ 2 params-index)
          for bindings-index = -1 then (+ 1 bindings-index)
          for end = (length params)
          for env = (make-env #() :outer toplevel-env) 
          then (let* ((item (aref bindings bindings-index))
                      (k (car item))
                      (v (cdr item)))
                 (env-set env k v))
          until (>= params-index end)
          do (let ((k (aref params params-index))
                   (v (if (= (+ 1 params-index) end) nil (aref params (+ 1 params-index)))))
               (if (zerop params-index)
                   (setf v (mal-eval v toplevel-env))
                 (setf v (mal-eval v env)))
               (vector-push-extend (cons k v) bindings))
          finally (return env))))

(defun mal-eval (ast env)
  (declare (type mal-env env))
  (cond ((listp ast)
         (let ((op (car ast)))
           (cond ((mal-symbol-eq op +mal-def!+)
                  (let ((k (second ast))
                        (v (mal-eval (third ast) env)))
                    (env-set env k v)
                    (env-get env k)))
                 ((mal-symbol-eq op +mal-let*+)
                  (let ((final-env (parse-let*-params (second ast) env)))
                    (mal-eval (third ast) final-env)))
                 (t (let* ((expr (eval-ast ast env))
                           (fn (car expr)))
                      (cond ((functionp fn)
                             ;; only when it's a symbol then apply it
                             (apply fn (cdr expr)))
                            ;; otherwise, throws the `eval-not-applicable-error` error
                            (t (eval-not-applicable-error fn))))))))
        (t (eval-ast ast env))))

(defun mal-print (forms)
  (pr-str forms t))

(defun rep (str)
  (mal-print (mal-eval (mal-read str) *repl-env*)))

(defun repl-loop ()
  (loop for line = (readline)
        while line
        do (when (and (string/= line +empty-string+)
                      (not (comments-line-p line)))
             (let ((result (handler-case (rep line)
                             ;; handle conditions here and return values back to `result`
                             (unmatched-token-error (c)
                               (format t "Error: expected '~a', got ~A~%"
                                       (expected-token c) (current-token c))
                               +empty-string+)
                             ((or 
				 unexpected-token-error
				 hashmap-value-missed-error
				 eval-not-applicable-error)
                                     (condition)
                               (setf (mal-source condition) line)
                               (princ condition)
                               +empty-string+)
                             (env-symbol-not-found-error (c)
                               (format t "'~a' not found.~%" (env-symbol c))
                               +empty-string+)
                             (error (c)
                               ;; print error information and continue to next loop anyhow
                               (print c)
                               (format t "~%")
                               +empty-string+))))
               (when (string/= result +empty-string+)
                 (format t "~A~%" result))
               #+ccl (sleep 0.5)
               )))
  (format t "~%"))

(defun main (&rest args)
  (declare (ignore args))
  (format t "Mal [Common Lisp]~%")
  #+ccl (sleep 0.5)
  (repl-loop))
