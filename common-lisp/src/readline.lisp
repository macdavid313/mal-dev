;;;; readline.lisp
;;;; use `cl-readline`, which is a wrapper for GNU Readline,
;;;; to support full line editing and command history .
(in-package :cl-user)
(defpackage mal.readline
  (:use :cl)
  (:export :readline))
(in-package :mal.readline)

(declaim (inline novelty-check))

(defun novelty-check (x y)
  (declare (type (vector character) x y))
  (string/= (string-trim " " x)
            (string-trim " " y)))

(defun readline (&optional (prompt "mal-user> "))
  (rl:readline :prompt prompt
	       :add-history t
	       :novelty-check #'novelty-check))
