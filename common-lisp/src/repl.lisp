;;;; repl.lisp
;;;; implement default read-eval-print-loop
(in-package :cl-user)
(defpackage mal.repl
  (:use :cl :mal.readline :mal.conditions :mal.reader :mal.printer)
  (:export :repl-loop)
  (:documentation "Implement default `read`, `eval`, `print` and `repl` functions,
make those functions more reusable."))
(in-package :mal.repl)

(declaim (inline mal-read mal-eval mal-print rep))

(defvar %empty-string% "")
(defvar %default-env% "")

(defun %mal-read% (str)
  (let ((reader (read-str str)))
    (unwind-protect (read-form reader)
      (unless (reader-eof-p reader)
        (unexpected-token-error (reader-peek reader) str)))))

(defun %mal-eval% (ast env)
  (declare (ignore env))
  ast)

(defun %mal-print% (forms)
  (pr-str forms))

#|
(defun rep (str reader evaluator env printer)
  (funcall printer
           (funcall evaluator (funcall reader str) env)))|#

(defun repl-loop (&key (reader #'mal.repl::%mal-read%)
                       (evaluator #'mal.repl::%mal-eval%)
                       (printer #'mal.repl::%mal-print%)
                       (env mal.repl::%default-env%))
  (labels ((rep (line)
             (funcall printer
                      (funcall evaluator (funcall reader line) env))))
    (loop for line = (readline)
          while line
          do (when (and (string/= line %empty-string%)
                        (not (comments-line-p line)))
               (let ((result (handler-case (rep line)
                               ;; handle conditions here and return values back to `result`
                               (unmatched-token-error (c)
                                 (format t "Error: expected '~a', got ~A~%"
                                         (expected-token c) (current-token c))
                                 %empty-string%)
                               ((or 
                               unexpected-token-error
                               hashmap-value-missed-error
                               eval-not-applicable-error) (condition)
                                 (setf (mal-source condition) line)
                                 (princ condition)
                                 %empty-string%)
                               (env-symbol-not-found-error (c)
                                 (format t "'~a' not found.~%" (env-symbol c))
                                 %empty-string%)
                               (error (c)
                                 ;; print error information and continue to next loop anyhow
                                 (print c)
                                 (format t "~%")
                                 %empty-string%))))
                 (when (string/= result %empty-string%)
                   (format t "~A~%" result))
                 #+ccl (sleep 0.5)
                 )))
    (format t "~%")))
