;;;; core.lisp
;;;; define an associative data structure ns (namespace) that maps symbols to functions. Move the numeric function definitions into this structure.
;;;; iterate through the core.ns structure and add (set) each symbol/function mapping to the REPL environment (repl_env).
(in-package :cl-user)
(defpackage mal.core
  (:use :cl :mal.conditions :mal.types :mal.reader :mal.printer)
  (:export :fetch-core.ns))
(in-package :mal.core)

(defparameter *core.ns*
  (make-array '(0) :element-type 'cons :adjustable t :fill-pointer t))

(defun fetch-core.ns () (copy-seq *core.ns*))

(defmacro mal-true-or-false (test &optional (true-value t))
  (cond (true-value
         `(if ,test +mal-true+ +mal-false+))
        ((mal-symbol-eq true-value +mal-true+)
         `(if (mal-symbol-eq ,test +mal-true+) +mal-true+ +mal-false+))))

(defmacro defmal-fn (key params &body body)
  (let* ((prefix "mal:")
         (key& (get-mal-symbol (cond ((symbolp key)
                                      (make-mal-symbol (string-downcase (symbol-name key))))
                                     ((stringp key) (make-mal-symbol key)))))
         (fname (get-mal-symbol (cond ((symbolp key) 
                                       (make-mal-symbol 
                                        (concatenate 'string prefix (string-downcase (symbol-name key)))))
                                      ((stringp key) (make-mal-symbol (concatenate 'string prefix key)))))))
    (multiple-value-bind  (remaining-forms declarations doc-string) 
        (alexandria:parse-body body :documentation t)
      (let ((definition `(defun ,fname ,params
                           ,(if doc-string
                                doc-string 
                              (format nil "#<Mal Procedure: ~a>" (string-downcase (symbol-name key))))
                            ,@declarations
                            ,@remaining-forms)))
        `(progn
           ,definition
           (vector-push-extend (cons (make-mal-symbol ,(symbol-name key&))
                                     (make-mal-procedure :fn #'(lambda (&rest args)
                                                                 (apply #',fname args))
                                                         :src "#<Compiled Function>"
                                                         :name ,(string-downcase (symbol-name key))))
                               *core.ns*))))))

(defmal-fn list (&rest things)
  (if (null things)
      +mal-empty-list+
    (apply #'list things)))

(defmal-fn list? (thing) 
  (mal-true-or-false 
   (or (listp thing) 
       (mal-symbol-eq thing +mal-empty-list+))))
  
(defmal-fn empty? (thing)
  (if (mal-symbol-eq thing +mal-empty-list+)
      +mal-true+
    (mal-true-or-false (alexandria:sequence-of-length-p thing 0))))

(defmal-fn count (lst) 
  (declare (type (or mal-symbol sequence) lst))
  (cond ((or (mal-symbol-eq lst +mal-empty-list+)
             (mal-symbol-eq lst +mal-nil+))
         0)
        (t (length lst))))

(defmal-fn = (a b)
  (labels ((sequencep (x)
             (or (listp x) (vectorp x) (mal-symbol-eq x +mal-empty-list+)))
           (->vector (x)
             (cond ((vectorp x) x)
                   ((listp x) (coerce x 'vector))
                   ((mal-symbol-eq x +mal-empty-list+) (vector))))
           (helper (a b)
             (cond ((or (stringp a) (stringp b)) (mal-true-or-false (equal a b)))
                   ((and (stringp a) (stringp b)) (mal-true-or-false (string= a b)))
                   ((and (numberp a) (numberp b)) (mal-true-or-false (= a b)))
                   ((or (and (mal-symbol-p a) (mal-symbol-p b))
                        (and (mal-keyword-p a) (mal-keyword-p b)))
                    (mal-true-or-false (mal-symbol-eq a b)))
                   ((mal-symbol-eq a +mal-empty-list+)
                    (mal-true-or-false (or (mal-symbol-eq b +mal-empty-list+)
                                           (and (or (listp a) (vectorp a)) (alexandria:emptyp b)))))
                   ((mal-symbol-eq b +mal-empty-list+)
                    (mal-true-or-false (or (mal-symbol-eq a +mal-empty-list+)
                                           (and (or (listp a) (vectorp a)) (alexandria:emptyp a)))))
                   ((and (sequencep a) (sequencep b))
                    (setf a (->vector a) b (->vector b))
                    (cond ((not (= (length a) (length b))) +mal-false+)
                          (t (loop for aa across a
                                   for bb across b
                                   do (when (eq (helper aa bb) +mal-false+)
                                        (return-from helper +mal-false+))
                                   finally (return +mal-true+)))))
                   ((and (hash-table-p a) (hash-table-p b))
                    (let ((alist1 (alexandria:hash-table-plist a))
                          (alist2 (alexandria:hash-table-plist b)))
                      (helper alist1 alist2)))
                   (t (mal-true-or-false (equal a b))))))
    (helper a b)))

(defmacro defmal-math-operator (&rest ops)
  `(progn
     ,@(loop for op in ops
             collect `(defmal-fn ,op (&rest nums)
                        (ecase ',op
                          ((+ - * /)
                           (apply #',op nums))
                          ((> >= < <=)
                           (mal-true-or-false (apply #',op nums))))))))

;;; Define multiple math operators
(defmal-math-operator + - * / > >= < <=)

;;; string functions
(defmal-fn pr-str (&rest objs)
  (string-join
   (map 'vector #'(lambda (o) (pr-str o t)) objs) 
   " "))

(defmal-fn str (&rest objs)
  (string-join 
   (map 'vector #'(lambda (x) (pr-str x nil)) objs) 
   ""))

(defmal-fn prn (&rest objs)
  (format t "~A~%"
          (string-join 
           (map 'vector #'(lambda (x) (pr-str x t)) objs) 
           " "))
  +mal-nil+)

(defmal-fn println (&rest objs)
  (format t "~A~%"
          (string-join 
           (map 'vector #'(lambda (x) (pr-str x nil)) objs) 
           " "))
  +mal-nil+)

;;; required by step6_file and later
(defmal-fn read-string (str)
  "This function just exposes the read_str function from the reader."
  (let ((reader (read-str str)))
    (unwind-protect (read-form reader)
      (unless (reader-eof-p reader)
        (unexpected-token-error (reader-peek reader) str)))))

(defmal-fn slurp (filename)
  "This function takes a file name (string) and returns the contents of the file as a string."
  (let ((pathname (uiop/pathname:ensure-pathname filename)))
    (alexandria:read-file-into-string pathname)))

;;; required by step7_quote and later
(defmal-fn cons (a lst)
  (cond ((or (null lst) (mal-symbol-eq lst +mal-empty-list+) (mal-symbol-eq lst +mal-nil+))
         (list a))
        ((consp lst) (cons a lst))
        ((vectorp lst) (cons a (coerce lst 'list)))))

(defmal-fn concat (&rest lsts)
  (cond ((null lsts) +mal-empty-list+)
        ((alexandria:sequence-of-length-p lsts 1)
         (let ((item (car lsts)))
           (if (or (mal-symbol-eq item +mal-empty-list+) (mal-symbol-eq item +mal-nil+))
               +mal-empty-list+
             item)))
        (t (apply #'concatenate 
                  'list 
                  (remove-if #'(lambda (x) 
                                 (or (mal-symbol-eq x +mal-empty-list+) (mal-symbol-eq x +mal-nil+)))
                             lsts)))))

;;; required by step8_macro and later
(defmal-fn nth (seq index)
  (let ((sequence (cond ((or (listp seq) (vectorp seq)) seq)
                        ((mal-symbol-eq seq +mal-empty-list+) (error "Empty list."))
                        (t (error "Wrong type for 1st argument of function 'nth'.")))))
    (elt sequence index)))

(defmal-fn first (seq)
  (cond ((or (mal-symbol-eq seq +mal-empty-list+) 
             (mal-symbol-eq seq +mal-nil+)
             (and (vectorp seq) (alexandria:emptyp seq)))
         +mal-nil+)
	((listp seq) (car seq))
        ((vectorp seq) (aref seq 0))))

(defmal-fn rest (seq)
  (cond ((listp seq) (cdr seq))
        ((vectorp seq)
	 (cond ((alexandria:emptyp seq) +mal-empty-list+)
	       (t (coerce (subseq seq 1) 'list))))
	((mal-symbol-eq seq +mal-empty-list+) +mal-empty-list+)
        (t (error "Runtime error for function 'rest'."))))

;;; time-ms, used by 'perf.ml' to test performance
(defmal-fn time-ms ()
  "Return a time values, (msec)."
  (get-internal-run-time))
