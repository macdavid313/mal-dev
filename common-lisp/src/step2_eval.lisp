;;;; step2_eval.lisp
;;;; In step 1 your mal interpreter was basically just a way to validate input and eliminate extraneous white space. 
;;;; In this step you will turn your interpreter into a simple number calculator by adding functionality to the evaluator (EVAL).
(in-package :cl-user)
(defpackage mal.step2
  (:use :cl :mal.readline :mal.conditions :mal.types :mal.reader :mal.printer)
  (:export :main))
(in-package :mal.step2)

(defvar +empty-string+ "")

(defun mal-read (str)
  (let ((reader (read-str str)))
    (unwind-protect (read-form reader)
      (unless (reader-eof-p reader)
        (unexpected-token-error (reader-peek reader) str)))))

(defparameter *repl-env*
  `((,(make-mal-symbol "+") . ,#'(lambda (&rest nums) (apply #'+ nums)))
    (,(make-mal-symbol "-") . ,#'(lambda (&rest nums) (apply #'- nums)))
    (,(make-mal-symbol "*") . ,#'(lambda (&rest nums) (apply #'* nums)))
    (,(make-mal-symbol "/") . ,#'(lambda (&rest nums) (apply #'/ nums)))))

(defun env-lookup (symbol &optional (env *repl-env*))
  (declare (type mal-symbol symbol))
  (let ((cons (assoc symbol *repl-env* :test 'mal-symbol-eq)))
    (if cons
        (cdr cons)
      (env-symbol-not-found-error symbol env))))

(defun eval-ast (ast env)
  (declare (type cons env)
           (optimize (speed 3) (safety 0) (space 0)))
  (cond ((or (numberp ast) (stringp ast)) ast)
	((mal-symbol-p ast)
         (cond (;; default symbols
                (mal-default-symbol-p ast)
                ast)
               (t (env-lookup ast env))))
	;; a list
	((listp ast) 
	 (map 'list #'(lambda (x) (mal-eval x env)) ast))
	;; a vector
	((vectorp ast)
	 (map 'vector #'(lambda (x) (mal-eval x env)) ast))
	;; a hash-table
	((hash-table-p ast)
	 (maphash #'(lambda (k v) 
		      (setf (gethash k ast) (mal-eval v env))) ast)
	 ;; remember to return the hash-table itself!
	 ast)
	(t ast)))

(defun mal-eval (ast env)
  (declare (type cons env))
  (cond ((listp ast)
         (let* ((expr (eval-ast ast env))
                (fn (car expr)))
           (if (functionp fn)
               ;; only when it's a symbol then apply it
               (apply fn (cdr expr))
             ;; otherwise, throws the `eval-not-applicable-error` error
             (eval-not-applicable-error fn))))
        (t (eval-ast ast env))))

(defun mal-print (forms)
  (pr-str forms t))

(defun rep (str)
  (mal-print (mal-eval (mal-read str) *repl-env*)))

(defun repl-loop ()
  (loop for line = (readline)
        while line
        do (when (and (string/= line +empty-string+)
                      (not (comments-line-p line)))
             (let ((result (handler-case (rep line)
                             ;; handle conditions here and return values back to `result`
                             (unmatched-token-error (c)
                               (format t "Error: expected '~a', got ~A~%"
                                       (expected-token c) (current-token c))
                               +empty-string+)
                             ((or 
				 unexpected-token-error
				 hashmap-value-missed-error
				 eval-not-applicable-error)
                                     (condition)
                               (setf (mal-source condition) line)
                               (princ condition)
                               +empty-string+)
                             (env-symbol-not-found-error (c)
                               (format t "'~a' not found.~%" (env-symbol c))
                               +empty-string+)
                             (error (c)
                               ;; print error information and continue to next loop anyhow
                               (print c)
                               (format t "~%")
                               +empty-string+))))
               (when (string/= result +empty-string+)
                 (format t "~A~%" result))
               #+ccl (sleep 0.5)
               )))
  (format t "~%"))

(defun main (&rest args)
  (declare (ignore args))
  (format t "Mal [Common Lisp]~%")
  #+ccl (sleep 0.5)
  (repl-loop))
