;;;; reader.lisp
(in-package :cl-user)
(defpackage mal.reader
  (:use :cl :mal.conditions :mal.types)
  (:import-from 
   :parse-number 
   :parse-positive-real-number :parse-real-number :parse-number)
  (:export 
   :read-str :read-form 
   :reader-peek :reader-next :reader-eof-p :comments-line-p)
  (:documentation "'read' the string from the user and parse it into an 
internal tree data structure (an abstract syntax tree)
and then take that data structure and 'print' it back to a string."))

(in-package :mal.reader)

(declaim (inline comments-line-p))

;;; `start` and `end` delimiters, for lists, vectors and hashmaps.
(defvar +list-start+ #\()
(defvar +list-end+ #\))
(defvar +vector-start+ #\[)
(defvar +vector-end+ #\])
(defvar +hashmap-start+ #\{)
(defvar +hashmap-end+ #\})

;;; Utilities
(defvar *float-format* 'double-float 
  "Parse all mal floating numbers as a double-float number.")

(defun str-is-number-p (str)
  "Predicate if a string is a number. If it's true return 't and parsed number,
otherwise return '(nil nil)."
  (declare (type string str))
  (flet ((char-is-number-p (char)
           (declare (type character char))
           (char<= #\0 char #\9)))
    (let ((dot-appeared-p)
          (sign)
          (len (length str)))
      (when (and (= len 1) (not (char-is-number-p (char str 0))))
        (return-from str-is-number-p (values nil nil)))
      (dotimes (i (length str))
        (let ((char (char str i)))
          (cond (;; check whether the first char is #\+ or #\- or a number
                 (zerop i)
                 (if (char= char #\-)
                     (setf sign char)
                   (unless (char-is-number-p char)
                     (return-from str-is-number-p (values nil nil)))))
                (;; check dots '#\.'
                 (char= char #\.)
                 (if dot-appeared-p (return-from str-is-number-p (values nil nil)) (setf dot-appeared-p t)))
                (;; if there's a non-number character, return '(nil nil) at once
                 (not (char-is-number-p char)) (return-from str-is-number-p (values nil nil))))))
      (let ((float-format *float-format*))
        (cond ((and (not sign) dot-appeared-p)
               (values t (parse-positive-real-number str :float-format float-format)))
              (dot-appeared-p (values t (parse-real-number str :float-format float-format)))
              (t (values t (parse-number str))))))))

;;; Reader Classes
(defun make-reader (tokens sum-of-tokens)
  "Initialization method for Reader class, which actually return a closure."
  (declare (type sequence tokens)
	   (type fixnum sum-of-tokens))
  (let ((index 0))
    #'(lambda (method)
        (cond ((eq method 'next) (if (= index sum-of-tokens)
                                     :eof
                                   (prog1 (elt tokens index)
                                     (incf index))))
              ((eq method 'peek) (if (= index sum-of-tokens)
                                     :eof
                                   (elt tokens index)))))))

(defmacro define-reader-method (name params &body body)
  "A helper macro that defines methods for Reader class."
  (multiple-value-bind  (remaining-forms declarations doc-string) 
      (alexandria:parse-body body :documentation t)
    `(defun ,name ,(cons 'reader params)
       ,(if doc-string
            doc-string 
          (concatenate 'string "[Reader Method]: " (symbol-name name) "."))
       ,@(cons '(declare (type function reader)) declarations)
       ,@remaining-forms)))
     
(define-reader-method reader-peek () 
  "Peek one token from the Reader instance."
  (funcall reader 'peek))

(define-reader-method reader-next () 
  "Retrun the current token form a Reader instance then increment position by 1."
  (funcall reader 'next))

(define-reader-method reader-eof-p ()
  "Test whether a Reader instance has already encountered :eof."
  (eq :eof (reader-peek reader)))

(define-reader-method read-form ()
  "This method will peek at the first token in the Reader instance and switch on the first character of that token. It 'dispatches' the Reader instance to other Reader methods in different cases."
  (declare (optimize (speed 3) (safety 0) (space 0)))
  (when (reader-eof-p reader) (return-from read-form nil))
  (let* ((token (reader-peek reader))
	 (char (char token 0)))
    (cond 
     ;; continue to read lists --> (read-list reader)
     ((char= char +list-start+) (reader-next reader) (read-list reader))
     ((char= char +list-end+) (unexpected-token-error token))

     ;; continue to read vectors --> (read-vector reader)
     ((char= char +vector-start+) (reader-next reader) (read-vector reader))
     ((char= char +vector-end+) (unexpected-token-error token))

     ;; continue to read hashmaps --> (read-hashmap reader)
     ((char= char +hashmap-start+) (reader-next reader) (read-hashmap reader))
     ((char= char +hashmap-end+) (unexpected-token-error token))

     ;; quote
     ((char= char #\') 
      (reader-next reader) 
      (list +mal-quote+ (read-form reader)))

     ;; quasiquote
     ((char= char #\`) 
      (reader-next reader) 
      (list +mal-quasiquote+ (read-form reader)))

     ;; splice-unquote
     ((string= token "~@") 
      (reader-next reader) 
      (list +mal-splice-unquote+ (read-form reader)))
     
     ;; unquote
     ((char= char #\~) 
      (reader-next reader) 
      (list +mal-unquote+ (read-form reader)))

     ;; with-meta
     ((char= char #\^) (reader-next reader) 
      (let ((hashmap (read-form reader))
            (vec (read-form reader)))
        (list +mal-with-meta+ vec hashmap)))

     ;; deref
     ((char= char #\@) 
      (reader-next reader) 
      (list +mal-deref+ (read-form reader)))

     ;; an atom
     (t (read-atom reader)))))

(define-reader-method read-to-end (end output &optional (result nil result-supplied-p))
  "Given a Reader instance, this function collect items from a 'thing' looks like sequence."
  (declare (type character end))
  (let ((token (reader-peek reader))
	(result (cond ((eq output 'list) nil)
		      (result-supplied-p result)
		      ((eq output 'vector) (make-array 0 :adjustable t :fill-pointer 0)))))
    (cond (;; expect the `end` delimiter instead of :eof
           (eq token :eof) 
           (unmatched-token-error token end))
	  (;; meet the `end` delimiter
           (char= (char token 0) end)
	   (reader-next reader)
	   (if (eq output 'list) (nreverse result) result))
          ;; accumulate items
	  (t (cond ((eq output 'list)
		    (cons (read-form reader) (read-to-end reader end 'list)))
		   ((eq output 'vector)
		    (vector-push-extend (read-form reader) result)
		    (read-to-end reader end 'vector result)))))))

(define-reader-method read-list ()
  "Read a list, e.g. (+ 1 2 3)."
  (let ((lst (read-to-end reader +list-end+ 'list)))
    (if lst lst +mal-empty-list+)))
    
(define-reader-method read-vector ()
  "Read a vector, e.g. [1 2 3]."
  (read-to-end reader +vector-end+ 'vector))

(define-reader-method read-hashmap ()
  "Read a hashmap, e.g. {\"a\" 1 \"b\" 2}, {:a 1 :b 2}."
  (declare (optimize (speed 3) (space 0) (safety 0)))
  (let ((lst (read-to-end reader +hashmap-end+ 'list))
        (table (make-hash-table :test 'equal)))
    ;; note: there's not explicit type checking for now ...
    (labels ((iter (lst result)
               (if (null lst)
                   result
                 (if (cdr lst)
                     (let ((k (first lst))
                           (v (second lst)))
                       (setf (gethash k result) v)
                       (iter (cddr lst) result))
                   (hashmap-value-missed-error (car lst))))))
      (iter lst table))))

(defun replace-all (string part replacement &key (test #'char=))
  ;;; it's borrowed from the online book `The Common Lisp Cookbook`
  "Returns a new string in which all the occurences of the part
is replaced with replacement."
  (with-output-to-string (out)
    (loop with part-length = (length part)
          for old-pos = 0 then (+ pos part-length)
          for pos = (search part string
                            :start2 old-pos
                            :test test)
          do (write-string string out
                           :start old-pos
                           :end (or pos (length string)))
          when pos do (write-string replacement out)
          while pos)))

(define-reader-method read-atom ()
  "Read an atom. For now, only booleans, keywords, nil, strings and numbers are considered."
  (let ((token (reader-next reader))
	(tmp))
    (cond
      ;; keyword
      ((char= (char token 0) #\:) (make-mal-symbol token :keywordp t))
      ;; true
      ((string= token "true") +mal-true+)
      ;; false
      ((string= token "false") +mal-false+)
      ;; nil
      ((string= token "nil") +mal-nil+)
      ;; empty list
      ((string= token "()") +mal-empty-list+)
      ;; string
      ((char= (char token 0) #\")
       (let ((end-index (1- (length token))))
         (cond (;; detect unmatched double quote
                (or (zerop end-index) ; the first and only the first char is #\"
                    (char/= (char token end-index) #\")) ; the last char is not #\"
                (unmatched-token-error :eof #\"))
               (t (replace-all (subseq token 1 end-index) "\\\"" "\"")))))
      ;; number (integer or float)
      ((multiple-value-bind (result number) (str-is-number-p token)
	 (and result (setf tmp number) t))
       tmp)
      ;; symbol
      (t (make-mal-symbol token)))))

;;; APIs
(defparameter *token-scanner*
  (ppcre:create-scanner
   "[\\s,]*(~@|[\\[\\]{}()'`~^@]|\"(?:[\\\\].|[^\\\\\"])*\"?|;.*|[^\\s\\[\\]{}()'\"`@,;]+)"))

(defvar +trim+
  (coerce (list #\Space #\Comma #\Newline) 'string))

(defun tokenizer (str)
  "Tokenize the input string, then return a vector of tokens."
  (let ((result (make-array 0 :element-type 'simple-string :adjustable t :fill-pointer 0))
	(end (length str))
	(sum-of-tokens 0))
    (ppcre:do-matches-as-strings
        ;; iterate over matched tokens, filter out 'incorrect' ones, finally
        ;; return the result as a vector and also the amount of tokens.
	(token *token-scanner* str (values result sum-of-tokens) :start 0 :end end :sharedp t)
      (let ((token (string-trim +trim+ token)))
	(when (and (string/= token "")
		   (char/= (char token 0) #\;))
	  (incf sum-of-tokens)
	  (vector-push-extend token result))))))

(defun comments-line-p (str)
  "Predicate whether a line is comment."
  (declare (type string str))
  (alexandria:emptyp (tokenizer str)))
			       
(defun read-str (str)
  "Read a string and return a corresponding Reader instance."
  (declare (type simple-string str))
  (multiple-value-bind (tokens sum-of-tokens) (tokenizer str)
    (make-reader tokens sum-of-tokens)))
