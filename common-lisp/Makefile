ifdef $LISP
	LISP=$($LISP)
else
	LISP="sbcl ecl ccl lispworks"
endif

CURRENT_PATH=${shell pwd}
STEPS=step0_repl step1_read_print step2_eval step3_env step4_if_fn_do \
      step5_tco step6_file step7_quote step8_macros #step9_try stepA_mal

all: mal
	mkdir -p bin && cd bin && \
	for step in $(STEPS); do \
	  ln -s mal $$step; \
	done

mal:
	cl --output ./bin/mal --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --eval '(push "$(CURRENT_PATH)/" asdf:*central-registry*)' \
	   --system cl-mal \
	   --system cl-launch/dispatch \
	   --dispatch-entry step0_repl/mal.step0:main \
	   --dispatch-entry step1_read_print/mal.step1:main \
	   --dispatch-entry step2_eval/mal.step2:main \
	   --dispatch-entry step3_env/mal.step3:main \
	   --dispatch-entry step4_if_fn_do/mal.step4:main \
	   --dispatch-entry step5_tco/mal.step5:main \
	   --dispatch-entry step6_file/mal.step6:main \
	   --dispatch-entry step7_quote/mal.step7:main \
	   --dispatch-entry step8_macros/mal.step8:main #\
	   #--dispatch-entry step9_try/mal.step9:main \
	   #--dispatch-entry stepA_mal/mal.stepA:main \
	   #--restart mal.stepA:main

step0_repl:
	cl --output ./bin/step0_repl --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/readline.lisp' \
	   --load 'src/step0_repl.lisp' \
	   --restart mal.step0:main

step1_read_print:
	cl --output ./bin/step1_read_print --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/step1_read_print.lisp' \
	   --restart mal.step1:main

step2_eval:
	cl --output ./bin/step2_eval --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/step2_eval.lisp' \
	   --restart mal.step2:main

step3_env:
	cl --output ./bin/step3_env --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/env.lisp' \
	   --load 'src/step3_env.lisp' \
	   --restart mal.step3:main

step4_if_fn_do:
	cl --output ./bin/step4_if_fn_do --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/env.lisp' \
	   --load 'src/core.lisp' \
	   --load 'src/step4_if_fn_do.lisp' \
	   --restart mal.step4:main

step5_tco:
	cl --output ./bin/step5_tco --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/env.lisp' \
	   --load 'src/core.lisp' \
	   --load 'src/step5_tco.lisp' \
	   --restart mal.step5:main

step6_file:
	cl --output ./bin/step6_file --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/env.lisp' \
	   --load 'src/core.lisp' \
	   --load 'src/step6_file.lisp' \
	   --entry mal.step6:main

step7_quote:
	cl --output ./bin/step7_quote --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/env.lisp' \
	   --load 'src/core.lisp' \
	   --load 'src/step7_quote.lisp' \
	   --entry mal.step7:main

step8_macro:
	cl --output ./bin/step8_macro --dump ! \
	   --lisp $(LISP) --quicklisp \
	   --system cl-ppcre \
	   --system parse-number \
	   --system cl-readline \
	   --eval '(declaim (optimize (speed 3) (space 0) (safety 0)))' \
	   --load 'src/conditions.lisp' \
	   --load 'src/readline.lisp' \
	   --load 'src/types.lisp' \
	   --load 'src/reader.lisp' \
	   --load 'src/printer.lisp' \
	   --load 'src/env.lisp' \
	   --load 'src/core.lisp' \
	   --load 'src/step8_macros.lisp' \
	   --entry mal.step8:main

clean:
	rm ./bin/*

debug: clean, all



