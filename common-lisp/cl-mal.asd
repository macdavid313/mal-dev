;;;; cl-mal.asdf
(defsystem cl-mal
  :author "David Guru, david_guru@gty.org.in"
  :description "Common Lisp Implementation of MAL(Make a Lisp)."
  :serial t
  :pathname "src/"
  :depends-on (:cl-readline
	       :cl-ppcre
	       :parse-number)
  :components ((:file "conditions")
	       (:file "readline")
               (:file "types")
	       (:file "reader")
	       (:file "printer")
               (:file "env")
               (:file "core")
	       (:file "step0_repl")
               (:file "step1_read_print")
               (:file "step2_eval")
               (:file "step3_env")
               (:file "step4_if_fn_do")
	       (:file "step5_tco")
	       (:file "step6_file")
	       (:file "step7_quote")
	       (:file "step8_macros")
               ))
